import os

for dirname, _, filenames in os.walk("/kaggle/input"):
    for filename in filenames:
        print(os.path.join(dirname, filename))

import numpy as np
import pandas as pd

train = pd.read_csv("/kaggle/input/hw-multiclass-classification/train.csv", sep=";")
test = pd.read_csv("/kaggle/input/hw-multiclass-classification/test.csv", sep=";")
submission = pd.read_csv("/kaggle/input/hw-multiclass-classification/sub_baseline.csv")
mapper = {"low": 0, "medium": 1, "high": 2}
train["interest_level"] = train["interest_level"].apply(lambda x: mapper[x])
train["features"] = train["features"].str.replace("['\[\]]", "").str.split(", ")
train["features_cnt"] = train["features"].apply(len)
train["photos"] = train["photos"].str.replace("['\[\]]", "").str.split(", ")
train["photos_cnt"] = train["photos"].apply(len)
target = "interest_level"
train["bedroom_price"] = train["price"] / train["bedrooms"]
train["bathroom_price"] = train["price"] / train["bathrooms"]
for i in range(len(train["display_address"])):
    if "St" in str(train["display_address"][i]) or "ST" in str(
        train["display_address"][i]
    ):
        train["display_address"][i] = 0
        continue
    if "Place" in str(train["display_address"][i]):
        train["display_address"][i] = 1
        continue
    if "Ave" in str(train["display_address"][i]):
        train["display_address"][i] = 2
        continue
    if "Park" in str(train["display_address"][i]):
        train["display_address"][i] = 3
        continue
    if "Boulevard" in str(train["display_address"][i]):
        train["display_address"][i] = 4
        continue
    train["display_address"][i] = 5

train.fillna(0)
train.replace([np.inf, -np.inf], 0, inplace=True)
# %%
features = [
    "bathrooms",
    "bedrooms",
    "latitude",
    "longitude",
    "features_cnt",
    "photos_cnt",
    "price",
    "bedroom_price",
    "bathroom_price",
    "display_address",
]

from sklearn.model_selection import (
    train_test_split,
    StratifiedKFold,
    RandomizedSearchCV,
    cross_val_score,
    cross_validate,
    GridSearchCV,
)
from sklearn.preprocessing import StandardScaler, Normalizer
from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from xgboost import XGBClassifier
from sklearn.metrics import (
    accuracy_score,
    f1_score,
    average_precision_score,
    roc_auc_score,
    classification_report,
    precision_score,
    recall_score,
    make_scorer,
)
import joblib
import matplotlib.pyplot as plt

X_train, X_test, y_train, y_test = train_test_split(
    train[features], train[target], test_size=0.3, random_state=42
)
clfs = [
    MLPClassifier(),
    DecisionTreeClassifier(),
    RandomForestClassifier(),
    AdaBoostClassifier(),
    QuadraticDiscriminantAnalysis(),
    GaussianNB(),
    KNeighborsClassifier(),
]

for i in range(len(clfs)):
    clf = clfs[i]
    clf.fit(X_train, y_train)
    predicts = clf.predict(X_test)
    print(clf, classification_report(y_test, predicts), sep="\n")

# %%
f1 = make_scorer(f1_score, average="macro")


# %%
def grid(a, b):
    global grsch
    grsch = GridSearchCV(a, b, cv=5, scoring=f1)
    grsch.fit(X_train, y_train)
    print(grsch.best_estimator_)
    print(grsch.best_params_)


def predicts_from_best(x):
    predicts = x.predict(X_test)
    print(x, classification_report(y_test, predicts), sep="\n")


# %%
params = {
    "criterion": ["gini", "entropy"],
    "max_depth": [10, 30, 50],
    "random_state": [1],
}
clf = DecisionTreeClassifier()
grid(clf, params)
best_dt = grsch.best_estimator_
predicts_from_best(best_dt)
# %%
params = {
    "algorithm": ["SAMME", "SAMME.R"],
    "n_estimators": [30, 50, 70],
    "random_state": [1],
}
clf = AdaBoostClassifier()
grid(clf, params)
best_ab = grsch.best_estimator_
predicts_from_best(best_ab)
# %%
params = {
    "criterion": ["gini", "entropy"],
    "n_estimators": [100],
    "max_depth": range(1, 30),
    "min_samples_split": [3],
    "random_state": [1],
}
clf = RandomForestClassifier()
grid(clf, params)
best_rf = grsch.best_estimator_
predicts_from_best(best_rf)
# %%
feature_importance = best_rf.feature_importances_
feature_importance_df = pd.DataFrame(
    {"features": features, "feature_importance": feature_importance}
)
feature_importance_df.sort_values("feature_importance", ascending=False)
# %%
importances = best_rf.feature_importances_
indices = np.argsort(importances)

plt.title("Feature Importances")
plt.barh(range(len(indices)), importances[indices], color="b", align="center")
plt.yticks(range(len(indices)), [features[i] for i in indices])
plt.xlabel("Relative Importance")
plt.show()
# %%
best_rf.fit(train[features], train[target])
test["features"] = (
    test["features"].astype(str).str.replace("[\[\]']", "").str.split(", ")
)
test["features_cnt"] = test["features"].apply(len)
test["photos"] = test["photos"].astype(str).str.replace("[\[\]']", "").str.split(", ")
test["photos_cnt"] = test["photos"].apply(len)
test["bedroom_price"] = test["price"] / test["bedrooms"]
test["bathroom_price"] = test["price"] / test["bathrooms"]
for i in range(len(test["display_address"])):
    if "St" in str(test["display_address"][i]) or "ST" in str(
        test["display_address"][i]
    ):
        test["display_address"][i] = 0
        continue
    if "Place" in str(test["display_address"][i]):
        test["display_address"][i] = 1
        continue
    if "Ave" in str(test["display_address"][i]):
        test["display_address"][i] = 2
        continue
    if "Park" in str(test["display_address"][i]):
        test["display_address"][i] = 3
        continue
    if "Boulevard" in str(test["display_address"][i]):
        test["display_address"][i] = 4
        continue
    test["display_address"][i] = 5

# %%
test.fillna(0)
test.replace([np.inf, -np.inf], 0, inplace=True)
# %%
test[target] = best_rf.predict(test[features])
test[["listing_id", target]].to_csv("Pigalov.csv", index=None)
