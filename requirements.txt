numpy~=1.26.1
pandas~=2.1.1
joblib~=1.3.2
matplotlib~=3.8.0
scikit-learn~=1.3.1
xgboost~=2.0.0